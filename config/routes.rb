SSRails::Application.routes.draw do
  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"
  devise_for :users

  scope "/bonfiretides" do
	  resources :users, :data_types
	end
end