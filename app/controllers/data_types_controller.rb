class DataTypesController < ApplicationController
  respond_to :html, :js, :json

  # GET /data_types
  # GET /data_types.json
  def index
    @types = DataType.all

    respond_with(@types)
  end

  # GET /data_types/1
  # GET /data_types/1.json
  def show
    @type = DataType.find(params[:id])

    respond_with(@type)
  end

  # GET /data_types/new
  # GET /data_types/new.json
  def new
    @type = DataType.new
    render :partial => 'form', :locals => {:type => @type}
  end

  # GET /data_types/1/edit
  def edit
    @type = DataType.find(params[:id])
  end

  # POST /data_types
  # POST /data_types.json
  def create
    @type = DataType.new(params[:data_type])

    respond_to do |format|
      if @type.save
        # respond_with(@type)
        # format.html { redirect_to @type, notice: 'Data type was successfully created.' }
        format.js
      else
        format.html { render action: "new" }
        format.json { render json: @type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /data_types/1
  # PUT /data_types/1.json
  def update
    @type = DataType.find(params[:id])

    respond_to do |format|
      if @type.update_attributes(params[:data_type])
        format.html { redirect_to @type, notice: 'Data type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /data_types/1
  # DELETE /data_types/1.json
  def destroy
    @type = DataType.find(params[:id])
    @id = params[:id]
    if @type.destroy
        respond_with(@id)
    end
  end
end
