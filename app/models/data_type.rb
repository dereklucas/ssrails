class DataType < ActiveRecord::Base
  has_many :items
  attr_accessible :description, :title
end
