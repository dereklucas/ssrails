class Item < ActiveRecord::Base
  belongs_to :data_type
  attr_accessible :description, :title
end
