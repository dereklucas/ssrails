class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :title
      t.text :description
      t.integer :data_type_id

      t.timestamps
    end
    add_index :items, :data_type_id
  end
end
